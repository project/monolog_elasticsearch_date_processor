<?php

namespace Drupal\Tests\monolog_elasticsearch_date_processor\Unit;

use Drupal\Tests\UnitTestCase;
use Drupal\monolog_elasticsearch_date_processor\MonologProcessorElasticsearchDate;
use Monolog\DateTimeImmutable;

/**
 * Test the processor.
 *
 * @group monolog_elasticsearch_date_processor
 */
class DateProcessorTest extends UnitTestCase {

  /**
   * Our processor.
   *
   * @var \Drupal\monolog_elasticsearch_date_processor\MonologProcessorElasticsearchDate
   */
  protected $processor;

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    parent::setUp();
    $this->processor = new MonologProcessorElasticsearchDate();
  }

  /**
   * Test that the processor works.
   */
  public function testProcessorNoDate() {
    $record = $this->getRecordAfterProcessor([]);
    self::assertArrayNotHasKey('elasticsearch_date', $record['extra']);
  }

  /**
   * Test that this works with monolog 2.
   */
  public function testProcessorMonolog2() {
    if (!class_exists('\Monolog\DateTimeImmutable')) {
      self::assertEquals(TRUE, TRUE);
      return;
    }
    $record = [
      'datetime' => new DateTimeImmutable(TRUE),
    ];
    $record = $this->getRecordAfterProcessor($record);
    self::assertNotEmpty($record['extra']['elasticsearch_date']);
  }

  /**
   * Helper.
   */
  protected function getRecordAfterProcessor($record) {
    // Just make sure the extra key is there.
    $record['extra'] = [];
    $processor = $this->processor;
    return $processor($record);
  }

}
