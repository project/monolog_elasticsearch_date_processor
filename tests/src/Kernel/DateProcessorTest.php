<?php

namespace Drupal\Tests\monolog_elasticsearch_date_processor\Kernel;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\KernelTests\KernelTestBase;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;
use Symfony\Component\DependencyInjection\Reference;

/**
 * Test the processor.
 *
 * @group monolog_elasticsearch_date_processor
 */
class DateProcessorTest extends KernelTestBase {

  const LOG_TAG = 'monolog_elasticsearch_date_processor_test';

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * A place to store things.
   *
   * @var string
   */
  protected $tempFile;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'user',
    'monolog',
    'monolog_elasticsearch_date_processor',
  ];

  /**
   * {@inheritdoc}
   */
  public function setUp() : void {
    $this->tempFile = sys_get_temp_dir() . DIRECTORY_SEPARATOR . uniqid() . '.txt';
    parent::setUp();
    $this->installEntitySchema('user_role');
    $this->logger = $this->container->get('logger.factory')->get(self::LOG_TAG);
  }

  /**
   * {@inheritdoc}
   */
  public function tearDown() : void {
    parent::tearDown();
    @unlink($this->tempFile);
  }

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    parent::register($container);

    $container->setParameter('monolog.channel_handlers', [
      'default' => [
        'stream_test',
      ],
    ]);

    $container->setParameter('monolog.processors', [
      'elasticsearch_date',
    ]);

    $container->register('monolog.json_formatter', JsonFormatter::class);

    $reference = new Reference('monolog.json_formatter');

    $level = 10;

    // For monolog version 3, we need to use something else than 10, for example
    // "DEBUG".
    if (class_exists('Monolog\Level')) {
      $level = 'DEBUG';
    }

    $container->register('monolog.handler.stream_test', StreamHandler::class)
      ->addArgument($this->tempFile)
      ->addArgument($level)
      ->addArgument('monolog.level.debug')
      ->addMethodCall('setFormatter', [$reference]);
  }

  /**
   * Test that this works on monolog 2.
   */
  public function testProcessorMonolog2() {
    if (!class_exists('\Monolog\DateTimeImmutable')) {
      self::assertEquals(TRUE, TRUE);
      return;
    }
    $this->doLogTest();
  }

  /**
   * Test that is works on monolog 1.
   */
  public function testProcessorMonolog1() {
    if (class_exists('\Monolog\DateTimeImmutable')) {
      self::assertEquals(TRUE, TRUE);
      return;
    }
    $this->doLogTest();
  }

  /**
   * Helper.
   */
  protected function doLogTest() {
    $message = 'Test 123';
    $this->logger->info($message);
    $json = json_decode(file_get_contents($this->tempFile));
    self::assertEquals($message, $json->message);
    self::assertNotEmpty($json->extra->elasticsearch_date);
  }

}
