<?php

namespace Drupal\monolog_elasticsearch_date_processor;

use Monolog\DateTimeImmutable;
use Monolog\Level;
use Monolog\LogRecord;

/**
 * MonologProcessorElasticsearchDate service for processing this way.
 */
class MonologProcessorElasticsearchDate {

  /**
   * Invoke the logging.
   *
   * @param array $record
   *   The record to be logged.
   *
   * @return array
   *   The record.
   */
  public function __invoke($record) {
    if (class_exists('Monolog\LogRecord') && $record instanceof LogRecord) {
      return $this->processLogRecord($record);
    }
    if (empty($record["datetime"])) {
      return $record;
    }
    $datetime = $record["datetime"];
    if ($datetime instanceof \DateTime) {
      $record['extra']['elasticsearch_date'] = $datetime->format('Y-m-d\TH:i:s.v\Z');
    }
    if ($datetime instanceof DateTimeImmutable) {
      $record['extra']['elasticsearch_date'] = $datetime->format('Y-m-d\TH:i:s.v\Z');
    }
    return $record;
  }

  /**
   * This is for monolog 3 basically.
   */
  protected function processLogRecord(LogRecord $record) {
    $data = $record->toArray();
    $data['extra']['elasticsearch_date'] = $record->datetime->format('Y-m-d\TH:i:s.v\Z');
    $new_record = new LogRecord(
      $data['datetime'],
      $data['channel'],
      Level::fromValue($data['level']),
      $data['message'],
      $data['context'],
      $data['extra']
    );
    return $new_record;
  }

}
